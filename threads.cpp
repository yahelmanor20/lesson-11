#include "threads.h"

void I_Love_Threads()
{
	cout << "i love threads!" << endl;
}

void call_I_Love_Threads()
{
	thread first(I_Love_Threads);
	first.join();
}

void printVector(vector<int> primes)
{
	/*for (size_t i = 0; i < primes.size(); i++)
	{
		cout << primes[i] << endl;
	}*/
	if(primes.size() != 0 ) 
		cout << primes[(int)primes.size()-1] << endl;
	
}

void getPrimes(int begin, int end, vector<int>& primes)
{
	bool isPrime = true;
	
	for (int i = begin; i < end; i++)
	{
		printVector(primes);
		for (int j = 2; j <= i / 2; j++)
		{
			if (i % j == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
			primes.push_back(i);
		isPrime = true;
	}
}
vector<int> callGetPrimes(int begin, int end)
{
	vector<int> primes;
	thread first(getPrimes, begin, end, ref(primes));
	
	first.join();

	return primes;
}

void writePrimesToFile(int begin, int end, ofstream & file)
{
	bool isPrime = true;
	for (int i = begin; i < end; i++)
	{

		for (int j = 2; j <= i / 2; j++)
		{
			if (i % j == 0)
			{
				isPrime = false;
				break;
			}
		}
		if (isPrime)
			file << i<<"\n";
		isPrime = true;
	}
}

void callWritePrimesMultipleThreads(int begin, int end, string filePath, int N)
{	
	ofstream file;
	file.open(filePath);
	thread first(writePrimesToFile, 0, N, ref(file));
	first.detach();
	for (int i = begin; i < end; i++)
	{
		if (i%N==0)
		{
			thread first(writePrimesToFile, i, i+N,ref(file));
			first.detach();
		}
	}
}
